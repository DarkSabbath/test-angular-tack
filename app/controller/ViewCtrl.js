'use strict';

angular.module('myApp')
    .controller('ViewCtrl', ['$uibModal', '$log', '$document', ViewCtrl]);

    function ViewCtrl ($uibModal, $log, $document) {
      var vm = this;
      vm.testVal = 'test';
      vm.groups = {
          public: ['test'],
          private: ['private test']
      };

      vm.addGroup = function (size, parentSelector) {
        var modalInstance = $uibModal.open({
          animation: false,
          ariaLabelledBy: 'modal-title-bottom',
          ariaDescribedBy: 'modal-body-bottom',
          templateUrl: 'view/ModalContent.html',
          controller: 'ModalCtrl',
          controllerAs: 'vm',
          size: 'sm',
          resolve: {
            groups: function () {
              return vm.groups;
            }
          }
        });
      };
    }
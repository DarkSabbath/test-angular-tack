angular.module('myApp').
    controller('ModalCtrl', ['$uibModalInstance', 'groups', ModalCtrl]);

    function ModalCtrl ($uibModalInstance, groups) {
      var vm = this;

      vm.type = '';
      vm.name = '';
      vm.groups = groups;
      console.log('vm.groups', vm.groups);

      vm.save = function () {
        console.log('vm.groups1', vm.groups);
        vm.groups[vm.type].push(vm.name);
        $uibModalInstance.close(vm.groups);
      };

      vm.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    };